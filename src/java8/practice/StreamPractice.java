package java8.practice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamPractice {

	public static void main(String arg[]){
//		System.out.println("Hello");
//		
//		System.out.println("Converting Java Stream to Collection or Array\n\n");
//		System.out.println("Stream to list");
//		Stream<Integer> stream = Stream.of(1,2,3,4);
//		List<Integer> list = stream.collect(Collectors.toList());
//		System.out.println(list);
//		
//		System.out.println("\nconverting Java stream to set");
//		stream = Stream.of(4,3,2,1);
//		Set<Integer> set = stream.collect(Collectors.toSet());
//		System.out.println(set);
//		
//		System.out.println("\nconverting Java stream to map");
//		stream = Stream.of(1,2,3,4);
//		Map<Integer, Integer> map = stream.collect(Collectors.toMap(i -> i, i->i*i));
//		System.out.println(map);
//		
//		System.out.println("\nconverting Java stream to array");
//		stream = Stream.of(1,2,3,4);
//		Integer arr[] = stream.toArray(Integer[]::new);
//		System.out.println(Arrays.toString(arr));
//		
//		System.out.println("\n\nJava Stream Intermediate Operations");
//		
//		System.out.println("\nStream filter() example");
//		List<Integer> listStream = new ArrayList<Integer>();
//		for(int i=0; i<100; i++) listStream.add(i);
//		Stream<Integer> streamList = listStream.stream();
//		
//		Stream<Integer> streamFilter = streamList.filter(p -> p>90);
//		System.out.println("Numbers greater than 90 are : "+streamFilter.collect(Collectors.toList()));
//		
//		System.out.println("\nStream map() example");
//		Stream<String> names = Stream.of("Ramu", "Lucky", "Ishita", "Mukund");
//		System.out.println(names.map(s->{
//				return s.toUpperCase();
//			}).collect(Collectors.toList()));
//		
//		System.out.println("\nStream sorted() example");
//		
//		System.out.println("normal sort");
//		names = Stream.of("Ramu","Lucky", "Ishita", "Mukund");
//		System.out.println(names.sorted().collect(Collectors.toList()));
//		
//		System.out.println("reverse sort");
//		names = Stream.of("Ramu","Lucky", "Ishita", "Mukund");
//		System.out.println(names.sorted(Comparator.reverseOrder()).collect(Collectors.toList()));
//		
//		System.out.println("\nStream flatMap() example");
//		Stream<List<String>> namesOriginalList = Stream.of(
//				Arrays.asList("Pankaj"), 
//				Arrays.asList("David", "Lisa"),
//				Arrays.asList("Amit"));
//			//flat the stream from List<String> to String stream
//			Stream<String> flatStream = namesOriginalList
//				.flatMap(strList -> strList.stream());
//
//			flatStream.forEach(System.out::println);
		
		System.out.println("\n\nJava Stream Terminal Operations");
		
		System.out.println("\nStream reduce() example");
		Stream<Integer> numbers = Stream.of(1,2,3,4,5);
		
		Optional<Integer> intOptional = numbers.reduce((i,j) -> {return i*j;});
		if(intOptional.isPresent()) System.out.println("Multiplication = "+intOptional.get());
		
		System.out.println("\nStream count() example");
		numbers = Stream.of(1,2,3,4,5);
		System.out.println("Stream count is "+ numbers.count());
		
		System.out.println("Stream forEach() example");
		numbers = Stream.of(1,2,3,4,5);
		numbers.forEach(i->System.out.print(i+" "));
		
		System.out.println("Stream match() examples");
		numbers = Stream.of(1,2,3,4,5);
		System.out.println("Stream contains 4? "+numbers.anyMatch(n->n==4));
		
		numbers = Stream.of(1,2,3,4,5);
		System.out.println("Stream contains all elements less than 10? "+numbers.allMatch(n -> n<10));
		
		numbers = Stream.of(1,2,3,4,5);
		System.out.println("Stream contains 10? "+numbers.noneMatch(n -> n==10));
		
		System.out.println("\nStream findFirst() example");
		Stream<String> names = Stream.of("Ramu", "Lucky", "Ishita", "Mukund");
		Optional<String> startWithM = names.filter(i -> i.startsWith("M")).findFirst();
		if(startWithM.isPresent()) System.out.println(startWithM.get());
	}
}
